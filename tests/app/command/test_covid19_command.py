from app.model.covid import PaysCovid19
from typing import Tuple
from unittest.mock import patch

from app.command import covid19_command
from app.model.pays import Pays
from tests.conftest import CommandTest


class TestLoadCountry(CommandTest):
    @patch('app.command.covid19_command.get_country')
    def test_load_should_not_fail_when_no_country_are_get(self, mock_country, runner):
        # Given
        mock_country.return_value = []
        # When
        runner.invoke(covid19_command.load_country, [], catch_exceptions=False)

        # Then
        assert Pays._get_collection().count_documents({}) == 0

    @patch('app.command.covid19_command.get_country')
    def test_load_should_update_each_country_received(self,
                                                      mock_country,
                                                      runner,
                                                      ref_pays: Tuple[Pays]):
        # Given
        france = ref_pays[0]
        england = ref_pays[1]
        mock_country.return_value = [
            PaysCovid19(**{'iso2': france.alpha2, 'slug': 'test_fr'}),
            PaysCovid19(**{'iso2': england.alpha2, 'slug': 'test_en'})
        ]
        # When
        runner.invoke(covid19_command.load_country, catch_exceptions=False)

        # Then
        france.reload()
        england.reload()
        assert Pays._get_collection().count_documents({}) == 5
        assert france.slug == 'test_fr'
        assert england.slug == 'test_en'
