from app.model.population import Population
from unittest.mock import patch

from app.command import referentiels_command
from app.model.pays import Pays
from tests.conftest import CommandTest


class TestLoadCountry(CommandTest):
    @patch('csv.reader')
    def test_load_empty_file_should_not_erase_entries(self, mock_reader, runner, ref_pays):
        # Given
        runner = self.app.test_cli_runner()
        mock_reader.return_value = iter([])
        # When
        runner.invoke(referentiels_command.load_country, [])

        # Then
        assert Pays._get_collection().count_documents({}) == 5

    @patch('csv.reader')
    def test_load_country_should_raise_error_when_file_has_no_header(self,
                                                                     mock_reader,
                                                                     runner,
                                                                     ref_pays):
        # Given
        runner = self.app.test_cli_runner()
        file_content = [('000', 'AA', 'AAA', 'a country', 'un pays')]
        mock_reader.return_value = iter(file_content)

        # When
        result = runner.invoke(referentiels_command.load_country, [])

        # Then
        assert "document Pays doesn't have fields" in str(result.exception)
        assert result.exit_code == 1
        assert Pays._get_collection().count_documents({}) == 5

    @patch('csv.reader')
    def test_load_country_should_raise_error_when_format_is_incorrect(self,
                                                                      mock_reader,
                                                                      runner,
                                                                      ref_pays):
        # Given
        runner = self.app.test_cli_runner()
        file_content = [
            ('code', 'alpha2', 'alpha3', 'unknown', 'label', 'libelle'),
            ('00  ', 'AA    ', 'AAA   ', '??     ', '??   ', '??     ')]
        mock_reader.return_value = iter(file_content)

        # When
        result = runner.invoke(referentiels_command.load_country, [])

        # Then
        assert "document Pays doesn't have fields" in str(result.exception)
        assert result.exit_code == 1
        assert Pays._get_collection().count_documents({}) == 5

    @patch('csv.reader')
    def test_load_country_should_replace_stored_Pays(self, mock_reader, runner, ref_pays):
        # Given
        runner = self.app.test_cli_runner()
        file_content = [
            ('code', 'alpha2', 'alpha3', 'label', 'libelle'),
            ('001', 'AA', 'AAA', 'Tatooine', 'Tatouine')]
        mock_reader.return_value = iter(file_content)

        # When
        result = runner.invoke(referentiels_command.load_country, [])

        # Then
        pays_saved = Pays.objects().first()
        assert result.exit_code == 0
        assert result.output.strip() == '. Done !'
        assert Pays._get_collection().count_documents({}) == 1
        assert pays_saved.code == 1
        assert pays_saved.alpha2 == 'AA'
        assert pays_saved.alpha3 == 'AAA'
        assert pays_saved.label == 'Tatooine'
        assert pays_saved.libelle == 'Tatouine'


class TestLoadPopulation(CommandTest):
    header = ['LocID', 'Location', 'VarID', 'Variant',
              'Time', 'MidPeriod', 'PopMale', 'PopFemale', 'PopTotal', 'PopDensity']

    @patch('csv.reader')
    def test_load_empty_file_should_not_erase_entries(self, mock_reader, runner, ref_population):
        # Given
        runner = self.app.test_cli_runner()
        mock_reader.return_value = iter([])
        # When
        runner.invoke(referentiels_command.load_population, [])

        # Then
        assert Population._get_collection().count_documents({}) == 1

    @patch('csv.reader')
    def test_load_country_should_raise_error_when_file_has_no_header(self,
                                                                     mock_reader,
                                                                     runner,
                                                                     ref_population):
        # Given
        runner = self.app.test_cli_runner()
        file_content = [
            ('4', 'France', '2', 'XXX', '2000', '2000.5', '3153', '3652.1', '6805.1', '55.874')
        ]
        mock_reader.return_value = iter(file_content)

        # When
        result = runner.invoke(referentiels_command.load_population, [])

        # Then
        assert "document Population doesn't have fields" in str(result.exception)
        assert result.exit_code == 1
        assert Population._get_collection().count_documents({}) == 1

    @patch('csv.reader')
    def test_load_country_should_raise_error_when_format_is_incorrect(self,
                                                                      mock_reader,
                                                                      runner,
                                                                      ref_population):
        # Given
        runner = self.app.test_cli_runner()
        bad_header = self.header.copy()
        bad_header[0] = 'bad column'
        file_content = [
            bad_header,
            ('4', 'France', '2', 'XXX', '2000', '2000.5', '3153', '3652.1', '6805.1', '55.874')
        ]
        mock_reader.return_value = iter(file_content)

        # When
        result = runner.invoke(referentiels_command.load_population, [])

        # Then
        assert "document Population doesn't have fields" in str(result.exception)
        assert result.exit_code == 1
        assert Population._get_collection().count_documents({}) == 1

    @patch('csv.reader')
    def test_load_country_should_replace_stored_Pays(self, mock_reader,
                                                           runner,
                                                           ref_population,
                                                           ref_pays):
        # Given
        ref_fr, _ = ref_pays
        runner = self.app.test_cli_runner()
        file_content = [
            self.header,
            (ref_fr.code, ref_fr.label, '2', 'XXX', '', '2000.5', '313', '362.1', '675.1', '55.87')
        ]
        mock_reader.return_value = iter(file_content)

        # When
        result = runner.invoke(referentiels_command.load_population, [])

        # Then
        pop_saved = Population.objects().first()
        assert result.exit_code == 0
        assert result.output.strip() == '. Done !'
        assert Population._get_collection().count_documents({}) == 1
        assert pop_saved.pays == ref_fr
        assert pop_saved.annee == 2000
        assert pop_saved.hommes == 313
        assert pop_saved.femmes == 362.1
        assert pop_saved.total == 675.1
        assert pop_saved.densite == 55.87
