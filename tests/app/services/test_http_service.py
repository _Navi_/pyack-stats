import requests
from unittest.mock import MagicMock, patch

from tests.conftest import UnitTest
from app.services.http_service import RequestConfig, _requests_factory, get, post


class TestRequestsFactory(UnitTest):
    def setup_method(self):
        self.retry = 3
        self.backoff_factor = 0.2

    def test_returns_a_new_request_session_object(self):
        # when
        new_request = _requests_factory(self.retry, self.backoff_factor)

        # then
        assert isinstance(new_request, requests.Session)

    @patch('app.services.http_service.Retry')
    def test_creates_retries_object(self, retry_mock):
        # when
        _requests_factory(self.retry, self.backoff_factor)

        # then
        retry_mock.assert_called_once_with(total=3, backoff_factor=0.2)

    @patch('app.services.http_service.Retry')
    @patch('app.services.http_service.HTTPAdapter')
    def test_creates_adapter_with_retries_capabilities(self, adapter_mock, retry_mock):
        # given
        retry = object()
        retry_mock.return_value = retry

        # when
        _requests_factory(self.retry, self.backoff_factor)

        # then
        _, kwargs_params = adapter_mock.call_args
        assert kwargs_params['max_retries'] == retry

    @patch('app.services.http_service.HTTPAdapter')
    @patch('app.services.http_service.requests.Session')
    def test_mounts_retry_mechanism_on_http_and_https_protocol(self, session_mock,
                                                               http_adapter_mock):
        # given
        session_mock.return_value = MagicMock()
        adapter = object()
        http_adapter_mock.return_value = adapter

        # when
        new_request = _requests_factory(self.retry, self.backoff_factor)

        # then
        args_list = new_request.mount.call_args_list
        first_mount_call_args_params, _ = args_list[0]
        assert first_mount_call_args_params[0] == 'http://'
        assert first_mount_call_args_params[1] is adapter

        second_mount_call_args_params, _ = args_list[1]
        assert second_mount_call_args_params[0] == 'https://'
        assert second_mount_call_args_params[1] is adapter


class TestGet(UnitTest):
    def setup_method(self):
        self.config = RequestConfig(
            retry=3,
            retry_factor=0.5,
            timeout=5,
            headers={'x-test': 'true'}
        )

    @patch('app.services.http_service._requests_factory')
    def test_call_requests_factory(self, factory_mock):
        # given
        url = 'http://url/api'
        client = MagicMock()
        factory_mock.return_value = client

        # when
        get(url, self.config)

        # then
        factory_mock.assert_called_once_with(self.config.retry, self.config.retry_factor)

    @patch('app.services.http_service._requests_factory')
    def test_update_query_header(self, factory_mock):
        # given
        url = 'http://url/api'
        client = MagicMock()
        client.headers = {}
        factory_mock.return_value = client

        # when
        get(url, self.config)

        # then
        assert self.config.headers == client.headers

    @patch('app.services.http_service._requests_factory')
    def test_returns_response_when_no_errors(self, factory_mock):
        # given
        url = 'http://url/api'
        client = MagicMock()
        response = MagicMock()
        client.get.return_value = response
        factory_mock.return_value = client

        # when
        result = get(url, self.config)

        # then
        client.get.assert_called_once_with(url, timeout=self.config.timeout, params={})
        response.raise_for_status.assert_called_once()
        assert response == result


class TestPost(UnitTest):
    def setup_method(self):
        self.config = RequestConfig(
            retry=3,
            retry_factor=0.5,
            timeout=5,
            headers={'x-test': 'true'}
        )

    @patch('app.services.http_service._requests_factory')
    def test_call_requests_factory(self, factory_mock):
        # given
        url = 'http://url/api'
        payload = {}
        client = MagicMock()
        factory_mock.return_value = client

        # when
        post(url, payload, self.config)

        # then
        factory_mock.assert_called_once_with(self.config.retry, self.config.retry_factor)

    @patch('app.services.http_service._requests_factory')
    def test_update_query_header(self, factory_mock):
        # given
        url = 'http://url/api'
        payload = {}
        client = MagicMock()
        client.headers = {}
        factory_mock.return_value = client

        # when
        post(url, payload, self.config)

        # then
        assert self.config.headers == client.headers

    @patch('app.services.http_service._requests_factory')
    def test_returns_response_when_no_errors(self, factory_mock):
        # given
        url = 'http://url/api'
        payload = {'nom': 'Neko'}
        client = MagicMock()
        response = MagicMock()
        client.post.return_value = response
        factory_mock.return_value = client

        # when
        result = post(url, payload, self.config)

        # then
        client.post.assert_called_once_with(url, data=payload, timeout=self.config.timeout)
        response.raise_for_status.assert_called_once()
        assert response == result
