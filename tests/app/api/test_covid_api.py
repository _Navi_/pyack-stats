from tests.conftest import ApiTest


class TestCovidApi(ApiTest):
    def test_get_data_api_default_response(self, client):
        # Given
        url = '/api/hello'

        # When
        rv = client.get(url)

        # Then
        returned_json = rv.get_json()
        assert rv.status_code == 200
        assert 'confirmed' in returned_json
        assert 'latest' in returned_json
