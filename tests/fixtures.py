from typing import Tuple
import pytest

from app.model.pays import Pays
from app.model.population import Population


@pytest.fixture
def ref_pays() -> Tuple[Pays]:
    en = Pays(
        code="826",
        alpha2="GB",
        alpha3="GBR",
        label="United Kingdom",
        libelle="Royaume-Uni",
        slug="united-kingdom").save()
    fr = Pays(
        code="250",
        alpha2="FR",
        alpha3="FRA",
        label="France",
        libelle="France",
        slug="france").save()
    Pays(code="372", alpha2="IE", alpha3="IRL", label="Ireland", libelle="Irl", slug="irl").save()
    Pays(code="056", alpha2="BE", alpha3="BEL", label="Belgium", libelle="Bel", slug="bel").save()
    Pays(code="276", alpha2="DE", alpha3="DEU", label="Germany", libelle="All", slug="ger").save()

    return (fr, en)


@pytest.fixture
def ref_population(ref_pays):
    ref_fr, _ = ref_pays
    p1 = Population(
        pays=ref_fr,
        annee=2020,
        hommes=100,
        femmes=100,
        total=200,
        densite=55.5
    ).save()

    return p1
