import pytest

from app.core import create_app
from app.core.database import get_db

from tests.fixtures import ref_pays, ref_population  # noqa: F401


@pytest.mark.unit_test
class UnitTest:
    pass


@pytest.mark.integration_test
class BaseTest:
    @classmethod
    def setup_class(cls):
        """
        Initialize flask app and configure it with a clean test database
        """
        test_config = cls._get_config()
        app = create_app(test_config)
        app.config['TESTING'] = True

        cls.ctx = app.app_context()
        cls.ctx.push()

        cls.app = app

    @staticmethod
    def _get_config():
        test_config = {
            'MONGODB_HOST': 'mongodb://127.0.0.1/test-covid19'
        }
        return test_config

    def setup_method(self, method):
        self._clean_db()

    def _clean_db(self):
        db = get_db()
        db_name = db.connection.get_default_database().name
        db.connection.drop_database(db_name)

    @classmethod
    def teardown_class(cls):
        cls.ctx.pop()


@pytest.mark.functional_test
class ApiTest(BaseTest):
    @pytest.fixture
    def client(self):
        yield self.app.test_client()


@pytest.mark.functional_test
class CommandTest(BaseTest):
    @pytest.fixture
    def runner(self):
        yield self.app.test_cli_runner()
