# README #

[![build-status](https://pipelines-badges-service.useast.staging.atlassian.io/badge/atlassian/pyack-stats.svg)](https://bitbucket.org/atlassian/pyack-stats/addon/pipelines/home)
[![Coverage Status](https://coveralls.io/repos/bitbucket/_Navi_/pyack-stats/badge.svg?branch=master)](https://coveralls.io/bitbucket/_Navi_/pyack-stats?branch=master)

**Live global stats (provided by [fight-covid19/bagdes](https://github.com/fight-covid19/bagdes)):**

![Covid-19 Confirmed](https://covid19-badges.herokuapp.com/confirmed/latest) ![Covid-19 Recovered](https://covid19-badges.herokuapp.com/recovered/latest) ![Covid-19 Deaths](https://covid19-badges.herokuapp.com/deaths/latest)

## What is this repository for? ##

This repository provides statistics on the COVID-19 pandemic. Datas are coming from [coronavirus-tracker-api](https://coronavirus-tracker-api.herokuapp.com) API and injected in internal database allowing doing some computing.

https://health.google.com/covid-19/open-data/raw-data
https://www.bing.com/covid/dev

## How do I get set up? ##

There is 2 options to setup the application on your environment :
* using your OS, possible if you have a bash interpreter
* using Docker

### Set up using your OS ###

First, you have to install [Python](https://www.python.org/downloads/) (version 3+).

Then, prepare your virtual environment for the application
```bash
make install
```

Finally, start the application
```bash
. var/venv/bin/activate
make start
```


### Set up with Docker ###

TODO => docker-compose up


## Contribution guidelines ##

TODO
* Writing tests
* Other guidelines


## Troubleshoot ##

error installing various package with message `[SSL: CERTIFICATE_VERIFY_FAILED], Command errored out with exit status 1: python setup.py egg_info`
--> ensure `setuptools` is installed and `pip` is up to date (>=20.0.0) : pip install --upgrade pip setuptools


## Who do I talk to? ##

ivan.femenia@gmail.com
