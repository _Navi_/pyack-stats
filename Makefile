.ONESHELL:

.PHONY: venv deploy start dev clean lint test coverage

install:
	@test -d .venv || python3 -m venv .venv
	. .venv/bin/activate && pip install -r requirements.txt
	@touch .venv/bin/activate

dev: install requirements-dev.txt
	. .venv/bin/activate && pip install -r requirements-dev.txt

start:
	FLASK_ENV=development flask run

clean:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +

lint:
	flake8 --extend-exclude=.venv

test: clean
	pytest tests/

coverage: clean
	pytest -q --cov=. --cov-report html tests/
