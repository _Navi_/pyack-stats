import collections
import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
import logging


logger = logging.getLogger(__name__)

RequestConfig = collections.namedtuple('RequestConfig', [
    'retry',
    'retry_factor',
    'timeout',
    'headers'
])


def _requests_factory(retry, backoff_factor):
    new_req = requests.Session()

    if retry and backoff_factor:
        retries = Retry(total=retry, backoff_factor=backoff_factor)
        adapter = HTTPAdapter(max_retries=retries)
        new_req.mount('http://', adapter)
        new_req.mount('https://', adapter)

    return new_req


def get(url: str, config: RequestConfig = RequestConfig, params={}):
    client = _requests_factory(config.retry, config.retry_factor)
    client.headers.update(config.headers)

    response = client.get(url, timeout=config.timeout, params=params)
    logger.debug(f'Call GET {url} ({response.status_code}) {response.elapsed}s')
    response.raise_for_status()

    return response


def post(url: str, payload, config: RequestConfig = RequestConfig):
    client = _requests_factory(config.retry, config.retry_factor)
    client.headers.update(config.headers)

    response = client.post(url, data=payload, timeout=config.timeout)
    logger.debug(f'Call POST {url} ({response.status_code}) {response.elapsed}s')
    response.raise_for_status()

    return response
