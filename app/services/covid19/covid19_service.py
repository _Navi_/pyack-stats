from app.model.covid import PaysCovid19
from datetime import datetime
import json
from typing import List

from .pays_validator import list_pays_validator
from app.services import http_service


class Covid19():
    def __init__(self) -> None:
        self.base_url = None
        self.request_config = None

    def init_app(self, app):
        self.base_url = app.config.get('COVID19_URL')
        self.request_config = http_service.RequestConfig(
            retry=app.config.get('COVID19_RETRY'),
            retry_factor=app.config.get('COVID19_BACKOFF_FACTOR'),
            timeout=app.config.get('COVID19_TIMEOUT'),
            headers={}
        )
        if not hasattr(app, 'extensions'):
            app.extensions = {}
        if 'covid_service' in app.extensions:
            raise RuntimeError('Extension already initialized')
        else:
            app.extensions['covid_service'] = self

    def _parse(self, raw):
        return json.loads(raw)

    def getCountries(self) -> List[PaysCovid19]:
        response = http_service.get(f'{self.base_url}/countries', self.request_config)
        return list_pays_validator.loads(response.text)

    def getStatsByCountry(self, country: str, depuis: datetime, to: datetime) -> str:
        filter = {}
        if depuis:
            filter['from'] = depuis.isoformat()
        if to:
            filter['to'] = to.isoformat()

        url = f'{self.base_url}/total/country/{country}'
        response = http_service.get(url, self.request_config, filter)
        return response.text


covi19 = Covid19()
get_country = covi19.getCountries
