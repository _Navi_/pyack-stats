
from marshmallow import Schema, fields, post_load, validate
from app.model.covid import PaysCovid19


class PaysCovid19Validator(Schema):
    label = fields.String(required=True, data_key='Country')
    slug = fields.String(required=True, data_key='Slug')
    iso2 = fields.String(required=True, data_key='ISO2', validate=[validate.Length(max=2)])

    @post_load
    def make_pays_covid19(self, data, **kwargs):
        return PaysCovid19(**data)


pays_validator = PaysCovid19Validator()
list_pays_validator = PaysCovid19Validator(many=True)
