from mongoengine import Document, fields

from .pays import Pays


class Population(Document):
    meta = {
        'indexes': [
            {'fields': ('pays', 'annee'), 'unique': True}
        ]
    }

    pays = fields.LazyReferenceField(
        Pays,
        required=True,
    )
    annee = fields.IntField(
        required=True,
        unique_with='pays',
        min_value=1950,
        max_value=2100
    )
    hommes = fields.FloatField()
    femmes = fields.FloatField()
    total = fields.FloatField()
    densite = fields.FloatField()
