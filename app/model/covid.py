from mongoengine import Document, fields

from .pays import Pays


class Covid19(Document):
    meta = {
        'indexes': [
            {'fields': ('pays', 'date'), 'unique': True}
        ]
    }

    pays = fields.LazyReferenceField(
        Pays,
        required=True,
    )
    date = fields.IntField(
        required=True,
        unique_with='pays'
    )
    contamines = fields.IntField(
        default=0,
        min_value=0
    )
    gueris = fields.IntField(
        default=0,
        min_value=0
    )
    deces = fields.IntField(
        default=0,
        min_value=0
    )


class PaysCovid19(Document):
    label = fields.StringField(required=True)
    slug = fields.StringField(required=True)
    iso2 = fields.StringField(required=True, max_length=2)
