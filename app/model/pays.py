from mongoengine import Document, fields


class Pays(Document):
    meta = {
        'indexes': [
            'label', 'alpha2'
        ]
    }

    code = fields.IntField(
        primary_key=True,
        regex="[0-9]{1,3}"
    )
    alpha2 = fields.StringField(
        required=True,
        unique=True,
        regex="[A-Z]{2}"
    )
    alpha3 = fields.StringField(
        required=True,
        unique=True,
        regex="[A-Z]{3}"
    )
    label = fields.StringField(
        required=True,
        unique=True
    )
    slug = fields.StringField()
    libelle = fields.StringField()
