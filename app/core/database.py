from flask_mongoengine import MongoEngine, current_mongoengine_instance


def create_db(app):
    return MongoEngine(app)


def get_db():
    return current_mongoengine_instance()
