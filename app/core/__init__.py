from flask import Flask

from app.api import blueprint
from app.command import referentiels_cli, covid_cli
from app.core.database import create_db


def create_app(test_config=None):
    app = Flask(__name__)

    if test_config is None:
        app.config.from_pyfile('../config.py')
    else:
        app.config.from_mapping(test_config)

    create_db(app)

    # register services
    from app.services.covid19.covid19_service import covi19
    covi19.init_app(app)

    # register apis
    app.register_blueprint(blueprint, url_prefix='/api')

    # register commands
    app.cli.add_command(referentiels_cli)
    app.cli.add_command(covid_cli)

    return app
