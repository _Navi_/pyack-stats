from app.core import create_app


if __name__ == '__main__':
    app = create_app()
    app.run(port=app.config.get('SERVER_PORT'), debug=app.config.get('DEBUG'))
