from .referentiels_command import referentiels_cli
from .covid19_command import covid_cli


__all__ = ('referentiels_cli', 'covid_cli', )
