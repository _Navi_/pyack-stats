import csv
import math

from flask.cli import AppGroup

from app.model.pays import Pays
from app.model.population import Population


referentiels_cli = AppGroup('referentiels')


@referentiels_cli.command('pays')
def load_country():
    with open('misc/countries.csv', 'r', encoding='utf-8') as fd:
        reader = csv.reader(fd, delimiter=',')
        headers = [h.strip() for h in next(reader)]
        unknown = [h for h in headers if h and not hasattr(Pays, h)]
        if unknown:
            raise ValueError("document Pays doesn't have fields %s" % unknown)

        Pays.drop_collection()

        for i, line in enumerate(reader):
            line_dict = {h: c for h, c in zip(headers, line) if h and c}
            doc = Pays(**line_dict)
            doc.save()
            if not i % 50:
                print('.', end='', flush=True)
        print(' Done !')


@referentiels_cli.command('population')
def load_population():
    expected_header = ('LocID', 'Location', 'VarID', 'Variant', 'Time', 'MidPeriod',
                       'PopMale', 'PopFemale', 'PopTotal', 'PopDensity')
    with open('misc/populations.csv', 'r', encoding='utf-8') as fd:
        reader = csv.reader(fd, delimiter=',')
        headers = [h.strip() for h in next(reader)]
        unknown = [h for h in headers if h not in expected_header]
        if unknown:
            raise ValueError("document Population doesn't have fields %s" % unknown)

        Population.drop_collection()
        documents = []
        cache = {}
        errors = set()
        for i, line in enumerate(reader):
            line_dict = {h: c for h, c in zip(headers, line) if h and c}
            if not line_dict['LocID'] in cache:
                pays = Pays.objects(code=line_dict['LocID'])
                if pays:
                    cache[line_dict['LocID']] = pays.first()
                else:
                    errors.add(f'Pays non trouvé {line_dict["LocID"]};{line_dict["Location"]}')
                    continue
            population = Population(
                pays=cache[line_dict['LocID']],
                annee=math.floor(float(line_dict['MidPeriod'])),
                hommes=line_dict.get('PopMale'),
                femmes=line_dict.get('PopFemale'),
                total=line_dict.get('PopTotal'),
                densite=line_dict.get('PopDensity')
            )
            documents.append(population)
            if not i % 500:
                Population.objects.insert(documents)
                documents = []
                print('.', end='', flush=True)
        print(' Done !')
        for error in errors:
            print(error)
