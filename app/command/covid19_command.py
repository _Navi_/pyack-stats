from flask.cli import AppGroup

from app.model.pays import Pays
from app.services.covid19.covid19_service import get_country


covid_cli = AppGroup('covid')


@covid_cli.command('pays')
def load_country():
    print('debut load_country')
    countries = get_country()
    print(countries)
    i = 0
    for country in countries:
        print(f'alpha2 = {country}')
        Pays.objects(alpha2=country.iso2).update_one(set__slug=country.slug)
        if not i % 20:
            print('.', end='', flush=True)
        i += 1
    print(' Done !')
