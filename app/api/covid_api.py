from http import HTTPStatus
from flask_restful import Resource

RESPONSE = {
    "confirmed": {
        "locations": [{
            "country": "France",
            "country_code": "FR",
            "province": "",
            "coordinates": {
                "lat": "46.2276",
                "long": "2.2137"
            },
            "history": {
                "1/22/20": 0,
                "1/23/20": 0,
                "1/24/20": 2,
            }
        }]
    },
    "latest": {
        "confirmed": 9609829,
        "deaths": 489312,
        "recovered": 4838921
    }
}


class CovidApi(Resource):
    def get(self):
        return RESPONSE, HTTPStatus.OK.value
