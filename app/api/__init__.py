from flask import Blueprint
from flask_restful import Api
from app.api.covid_api import CovidApi


blueprint = Blueprint('api', __name__)

api = Api(blueprint)
api.add_resource(CovidApi, '/hello')


__all__ = ('api')
