from app.model.pays import Pays


def fetch_by_iso2(alpha2: str) -> Pays:
    pays = Pays.objects(alpha2=alpha2)
    return pays.first() if len(pays) > 0 else None


def update(pays: Pays, payload: dict) -> Pays:
    pays.update(**payload)
    pays.save()
    return pays
