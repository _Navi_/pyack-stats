from os import environ


# Api
SERVER_PORT = environ.get('API_PORT', '9000')
DEBUG = environ.get('DEBUG') in ('true', 't', 'yes', 'y', 'oui', 'o', '1')

# MongoDB
DB_TYPE = environ.get('DB_TYPE', 'mongodb')
DB_NAME = environ.get('DB_NAME', 'covid19')
DB_HOST = environ.get('DB_HOST', 'localhost')
DB_PORT = environ.get('DB_PORT', '27017')
MONGODB_HOST = DB_TYPE + '://' + DB_HOST + ':' + DB_PORT + '/' + DB_NAME

# Covid19 API
COVID19_URL = 'https://api.covid19api.com'
COVID19_TIMEOUT = 30
COVID19_RETRY = 3
COVID19_BACKOFF_RETRY = 0.5
