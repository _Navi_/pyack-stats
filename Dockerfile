FROM python:3.6-slim

WORKDIR /src
COPY requirements.txt /src
RUN pip install -r requirements.txt

COPY app app

CMD ["flask", "run"]
